gitea-spammerblock
==================

This project holds the "spammer block bot" for gitea sign-up, except
for the database access script (`$HOME/rdb.sh`) and the control lists.

installation
------------

This is currently installed at `~git/src/spammerblock` and run in
situ, except that the `vault` with the control lists needs to be
opened and left open.

The `gitea.devuan.dev` project has an encrypted file in its `vault`
with the passsword required for this project `vault`.

I.e., you need to open the vault of that project (`gitea.devuan.dev`),
and then look into or run `../setup/temp/spammerblock.sh` for opening
the spammerblock vault here.
