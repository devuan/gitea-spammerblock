#!/bin/bash
#
# Find the sign-up IP and check against "bad IP list"

CMD=( grep /user/avatar/$NAME/-1 /var/log/nginx/access.log )
IP=$(sudo -u www-data ${CMD[@]} | awk '{print $1; exit;}')
if [ -z "$IP" ] ; then
    #echo "No IP found for $NAME"
    exit 0 # No IP found
fi

# Capture the IP for onward check(s)
echo "IP=$IP" >&4

exit 0
