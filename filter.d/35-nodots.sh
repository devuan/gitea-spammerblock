#!/bin/bash
#
# Disallow usernames with many dots

echo "EMAIL=$EMAIL" >&2
DOTS="$(echo "${EMAIL%@*}" | sed 's/[^\.]//g')"
if [ ${#DOTS} -gt 1 ] ; then
   echo "lots of dots $EMAIL"
   exit 1
fi
true
