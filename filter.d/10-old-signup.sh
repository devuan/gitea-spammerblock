#!/bin/bash
#
# The filter scripts have details of a user in the environment:
# ID NAME EMAIL WEBSITE CREATED ISACTIVE DESCRIPTION
#
# Return 1 for the decision of disabling the user
# Return 0 otherwise
#
# This signals anyone not activated within $ACTIVE_CODE_LIVE_MINUTES
# (from /etc/gitea/app.ini) and some as spammer.

[ "$ISACTIVE" = t ] && exit 0

GRACE=900

A="$(grep ACTIVE_CODE_LIVE_MINUTES /etc/gitea/app.ini | sed 's/^[^=]=//')"
A=$(( ${A:-180} + ${CREATED:-0} + ${GRACE} ))

NOW="$(date +%s)"
B="$(echo "$NOW > $A" | bc -l)"

if [ "$B" = 1 ] ; then
   echo "$NAME ($EMAIL) not active at $NOW since $CREATED"
   exit 1
fi

grep -q "$ID $NAME $EMAIL $CREATED" accepted.dat && exit 0

if grep " $EMAIL " accepted.dat ; then
    echo "Repeated registration attempt rejected."
    exit 1
fi

exit 0
