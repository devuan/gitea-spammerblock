#!/bin/bash
#
# Send email about accepted sign-up

grep -q "$ID $NAME $EMAIL" accepted.dat && exit 0

[ -z "$IP" ] && IP="not known"
if [ -z "$IP" ] ; then
    cat <<EOF
Not accepted without IP
ID=$ID
NAME=$NAME
EMAIL=$EMAIL
IP=$IP
WEBSITE=$WEBSITE
DESCRIPTION=$DESCRIPTION
EOF
    exit 1
fi

echo "$ID $NAME $EMAIL $CREATED" >> accepted.dat

FROM=git@git.devuan.org
ADMIN=( ralph.ronnquist@gmail.com )

function sendmail() {
    EMAIL=$FROM mutt -s "gitea spammer hammer: accepted sign-up $ID" ${ADMIN[@]}
}

cat <<EOF | sendmail
ID=$ID
NAME=$NAME
EMAIL=$EMAIL
IP=$IP
WEBSITE=$WEBSITE
DESCRIPTION=$DESCRIPTION
EOF

exit 0
