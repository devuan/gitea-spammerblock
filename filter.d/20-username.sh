#!/bin/bash
#
# Find username by checking for:
# allowed names in username_allowlist.txt
# denied name patterns in pattern_denylist.txt

#echo "username.sh: NAME=$NAME" >&2
grep -q "^$NAME\$" lists/username_allowlist.txt && exit 0
P="$(grep -v '#' lists/pattern_denylist.txt | tr '\012' '|' | sed 's/|$//')"
if echo "$NAME $EMAIL $WEBSITE $DESCRIPTION" | grep -oE "$P" ; then
    #echo "$P" >&2
    echo "Bad pattern in: $NAME $EMAIL $WEBSITE $DESCRIPTION"
    exit 1
fi
exit 0
