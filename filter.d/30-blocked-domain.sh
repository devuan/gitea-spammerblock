#!/bin/bash
#

X="$(echo "${EMAIL##*@}" | tr -d '[a-zA-z0-9._]')"

if [ ! -z "$X" ] ; then
    echo "ugly email domain $EMAIL"
    exit 1
fi

if grep -o "^${EMAIL#*@}\$" lists/domain_blocklist.txt ; then
    echo "blocked email $EMAIL"
    exit 1
fi
exit 0
