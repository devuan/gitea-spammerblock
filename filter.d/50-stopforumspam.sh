#!/bin/bash
#
# Check ip and email with stopforumspam
#

MD5=( $(echo "$EMAIL" | md5sum) )
R="$(curl -s "http://api.stopforumspam.org/api?email=$EMAIL")"
[ -z "$IP" ] || R+="&ip=$IP"
if echo "$R" | grep 'appears>yes</appears' ; then
    echo "$R"
    echo stopforumspam.sh $NAME $EMAIL
    exit 1
fi
sleep 1 # ensure requests spaced out
exit 0
