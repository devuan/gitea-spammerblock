# Functions for run-filter.sh and friends
#
# Filter scripts sit in filter.d/nn-*.sh which are tried in order.  A
# filter succeeds if ok, and fails if there is an issue.  A filter may
# provide environment assignments for subsequent filters by writing
# them to &4.

export FIELDS="id,name,email,website,created_unix,prohibit_login,is_active,description"

function spammer_action() {
    X="update public.user set prohibit_login=true where id=$ID;"
    echo "$X" >> $TMP
    $HOME/rdb.sh "$X" >> $TMP
    $HOME/delete-user.sh "$NAME" "$ID" >> $TMP 2>&1
}

function filtering() {
    export ID NAME EMAIL WEBSITE CREATED DESCRIPTION IP ISACTIVE
    while IFS='|' read id name email website created prohibit act desc ; do
	[ "$prohibit" = " t" ] && continue
	ID="$(echo $id )"
	[ -z "$ID" ] && continue
	name=( $(echo $name ) )
	NAME="${name[*]}"
	EMAIL="$(echo $email )"
	WEBSITE="$(echo $website )"
	CREATED="$(echo $created )"
	ISACTIVE="$(echo $act)"
	desc=( $desc )
	DESCRIPTION="${desc[*]}"
	IP=
	if [ -z "$TEST" ] ; then
	    grep -q "$ID $NAME $EMAIL" filtered.dat && continue
	fi
	BADDIE=false
	for SCRIPT in filter.d/[0-9][0-9]-*.sh ; do
	    [ -z "$TEST" ] || echo "Run $SCRIPT with $NAME" >&2
	    if $SCRIPT >> $TMP 4> $TMP.ENV ; then
		eval $(cat $TMP.ENV) >&2
	    else
	       [ -z "$TEST" ] || echo "$SCRIPT detected an issue" >&2
	       BADDIE=true
	       break
	    fi
	done
	if $BADDIE ; then
	    echo "$ID $NAME $EMAIL" >> filtered.dat
	    echo "$SCRIPT" >> $TMP
	    spammer_action
	fi
    done
}
