#!/bin/bash
#
# Open this vault by mounting it with gocryptfs onto ../temp
#

ROOT=$(git rev-parse --show-toplevel)
[ -z "$ROOT" ] && exit -1

SRC=$ROOT/vault
DST=$ROOT/temp

# The vault is open ... close it
exec fusermount -z -u $DST
