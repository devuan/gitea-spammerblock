#!/bin/bash
#
# Uses ./rdb.sh to run queries against new users who are not already
# prohibited.

ADMIN=( ralph.ronnquist@gmail.com )

##### 
{
    flock -n 9 || exit 0

    
    cd $(dirname $0)
    MARK=$(date -d '-7 day' +%s)

    TMP=$(mktemp)
    trap "rm -f $TMP $TMP.ENV" 0 2 15

    . ./functions.sh

    WHERE="type=0 and created_unix > $MARK"
    SELECT="select $FIELDS from public.user where $WHERE;"
    $HOME/rdb.sh "$SELECT" | filtering 2>/dev/null

    [ $(wc -l < $TMP) -eq 0 ] && exit 0
    IDS=( $(grep -oE "id=[0-9]*" $TMP | sed 's/id=//') )
    SUBJECT="gitea spammer hammer: filtered ${IDS[*]}"
    echo "cheers" | EMAIL="git@git.devuan.org" \
			 mutt -s "$SUBJECT" -i "$TMP" ${ADMIN[@]}

}  9< $0
